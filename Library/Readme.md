# Integrated Skills

## Training Resource

### AutomatedTesting

A Testable Component

- AutomatedTesting.dll
- AutomatedTesting.pdb
- AutomatedTesting.xml
- AutomatedTesting.deps.json

#### IAverages Interface

___

Add samples to the averager

`double Push(double)`

```csharp
   var avg = averages.Push(12.3456);
```

`double Push(params double[])`

```csharp
   var avg = averages.Push(1, 2, 3);
```

`double Push(IEnumerable<double>)`

```csharp
   var avg = averages.Push(new List<double>() { 4, 5, 6 });
```

___

Returns the current calculated average

`double Current { get; }`

```csharp
   var avg = averages.Current;
```

___

#### Implementations

##### RollingAverages

Provide an implementation of a rolling average algorithm using an definable number of samples.

###### Constructor

`RollingAverages(int bucketCount)`

`int` _bucketCount_ Number of sample buckets to hold

###### Example

```csharp
IAverages averages = new RollingAverages(bucketCount: 200);

do
{
  double sample = sensorAdapter.ReadDouble(PROBE_MV);

  double avg = averages.Push(sample);

  if(avg < minThreshold || avg > maxThreshold) {
    ...
  }
  ...
} while(true);
```

##### MostAverages

Provide an implementation of a most average algorithm.

###### Example

```csharp
IAverages averages = new MostAverages();

double previousMost = 0.0;

do
{
  double sample = sensorAdapter.ReadDouble(PROBE_MV);

  double most = averages.Push(sample);

  if(most <> previousMost) {
    previousMost = most;
    ...
  }
  ...
} while(true);
```
