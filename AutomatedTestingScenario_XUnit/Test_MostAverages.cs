using System;
using Xunit;
using AutomatedTesting;

namespace AutomatedTestingScenario_XUnit
{
    /// <summary>
    /// Tests to validate RollingAverages implementation
    /// </summary>
    public class Test_MostAverages
    {
        [Fact]
        public void Test1()
        {
            IAverages subject = new MostAverages();

            double avg = subject.Push(4);

            Assert.Equal(4, avg);
        }
    }
}
