using AutomatedTesting;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AutomatedTestingScenario_MSTest
{
    [TestClass]
    public class Test_MostAverages
    {
        [TestMethod]
        public void TestMethod1()
        {
            IAverages subject = new MostAverages();

            double avg = subject.Push(4);

            Assert.AreEqual(4, avg);
        }
    }
}
