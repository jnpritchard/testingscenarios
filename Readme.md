# Integrated Skills

## Training Resources

### Structure

#### AutomatedTestingScenario_NUnit

NUnit Test Project - Basis for creating tests of averages

#### AutomatedTestingScenario_XUnit

XUnit Test Project - Basis for creating tests of averages

#### AutomatedTestingScenario_MSTest

MSTest Test Project - Basis for creating tests of averages

## Useful Tools

- Fine Code Coverage VS Extension

