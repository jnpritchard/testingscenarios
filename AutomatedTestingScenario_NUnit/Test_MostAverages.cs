using AutomatedTesting;
using NUnit.Framework;

namespace AutomatedTestingScenario_NUnit
{
    public class Test_MostAverages
    {
        [Test]
        public void Test1()
        {
            IAverages subject = new MostAverages();

            double avg = subject.Push(4);

            Assert.AreEqual(4, avg);

            Assert.Pass();
        }
    }
}