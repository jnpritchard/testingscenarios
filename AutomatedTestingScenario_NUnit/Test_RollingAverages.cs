using AutomatedTesting;
using NUnit.Framework;

namespace AutomatedTestingScenario_NUnit
{
    public class Test_RollingAverages
    {
        [Test]
        public void Test1()
        {
            IAverages subject = new RollingAverages();

            double avg = subject.Push(4);

            Assert.AreEqual(4, avg);

            Assert.Pass();
        }
    }
}